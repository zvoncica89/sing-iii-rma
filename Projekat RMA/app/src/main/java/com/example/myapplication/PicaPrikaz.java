package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.time.Clock;
import java.util.ArrayList;
import java.util.HashMap;

public class PicaPrikaz extends AppCompatActivity {
    private ArrayList<Pice> pica;
    private ArrayList<LinearLayout> children;
    LinearLayout mainLayout;
    LinearLayout subLayout;
    TextView imePica, detaljiPica, cenaPica;
    HashMap<String, Integer> naruceno = new HashMap<>();
    Boolean prijavljen;
    Button narucenoPice;
    Button svaPica;
    Button sviKorisnici;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pica_prikaz);
        children = new ArrayList<>();
        pica = (ArrayList<Pice>) getIntent().getSerializableExtra("pica");
        prijavljen = getIntent().getBooleanExtra("prijavljen", false);
        narucenoPice = findViewById(R.id.naruceno);
        svaPica = findViewById(R.id.svaPica);
        sviKorisnici = findViewById(R.id.sviKorisnici);
        if (prijavljen == false){
            narucenoPice.setText(R.string.naruceno);
            svaPica.setVisibility(View.INVISIBLE);
            sviKorisnici.setVisibility(View.INVISIBLE);
            generateData();
        }else{
            narucenoPice.setVisibility(View.INVISIBLE);
            svaPica.setText(R.string.pica);
            sviKorisnici.setText(R.string.korisnici);
            svaPica.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    generateData();
                }
            });
        }



        narucenoPice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prikaziPoruceno();
            }
        });
    }

    private void generateData(){
        LayoutInflater inflater = LayoutInflater.from(this);
        mainLayout = findViewById(R.id.mainLayout);

        Button naruci;

        for(final Pice p : pica){
            System.out.println(p.getIme());
            subLayout = (LinearLayout) inflater.inflate(R.layout.pice_inflater, mainLayout, false);
            children.add(subLayout);

            imePica = subLayout.findViewById(R.id.imePica);
            detaljiPica = subLayout.findViewById(R.id.detaljiPica);
            cenaPica = subLayout.findViewById(R.id.cenaPica);
            naruci = subLayout.findViewById(R.id.dugmeNaruciPice);

            imePica.setText(p.getIme());
            detaljiPica.setText(p.getDetalji());
            cenaPica.setText(Double.toString(p.getCena()));

            if (prijavljen == false){
                naruci.setText(getString(R.string.naruci));
                naruci.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int narucenoPice = pica.indexOf(p);
                        String imeNarucenogPica = pica.get(narucenoPice).getIme();
//                    Toast.makeText(PicaPrikaz.this, imeNarucenogPica, Toast.LENGTH_SHORT).show();
                        if (naruceno.containsKey(imeNarucenogPica)){
//                            System.out.println(naruceno.get(imeNarucenogPica));
//                            int staraVrednost = naruceno.get(imeNarucenogPica);
                            naruceno.put(imeNarucenogPica, naruceno.get(imeNarucenogPica) + 1);
//                            System.out.println(naruceno.get(imeNarucenogPica));
                        }else {
                            naruceno.put(imeNarucenogPica, 1);
                        }
                    }
                });
            }else {
                naruci.setText(getString(R.string.izmeni));
            }
            mainLayout.addView(subLayout);
        }
    }

    private void prikaziPoruceno(){
        String poruceno = "";
        for (Object n : naruceno.keySet()){
            System.out.println(n + " " + n.toString());
            poruceno += n + " " + naruceno.get(n.toString()) + "; ";
        }
        Toast.makeText(this, poruceno, Toast.LENGTH_SHORT).show();
    }
}
