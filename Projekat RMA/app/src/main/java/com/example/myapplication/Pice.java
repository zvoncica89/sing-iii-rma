package com.example.myapplication;

import java.io.Serializable;

public class Pice implements Serializable {
    private String ime, detalji, vrsta;
    private int kolicina;
    private double cena;

    public Pice(String ime, String detalji, String vrsta, int kolicina, double cena) {
        this.ime = ime;
        this.detalji = detalji;
        this.vrsta = vrsta;
        this.kolicina = kolicina;
        this.cena = cena;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getDetalji() {
        return detalji;
    }

    public void setDetalji(String detalji) {
        this.detalji = detalji;
    }

    public String getVrsta() {
        return vrsta;
    }

    public void setVrsta(String vrsta) {
        this.vrsta = vrsta;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }
}
