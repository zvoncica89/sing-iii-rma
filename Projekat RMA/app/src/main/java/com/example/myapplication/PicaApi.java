package com.example.myapplication;

import java.util.ArrayList;

public class PicaApi {
    private static ArrayList<Pice> pica = new ArrayList<>();

    public static ArrayList<Pice> create(){
        Pice p1 = new Pice("Coca-Cola","","sok",150,125.0);
        Pice p2 = new Pice("Ice-tea","Hladan ledeni caj","sok",100,153.0);
        Pice p3 = new Pice("Erdinger","Psenicno pivo","alkoholno pice",50,170.0);
        Pice p4 = new Pice("Nes kafa","","topli napici",250,120.0);
        pica.add(p1);
        pica.add(p2);
        pica.add(p3);
        pica.add(p4);
        return pica;
    }

    public static ArrayList<Pice> getAll(){
        return pica;
    }

    public static void addOne(Pice novoPice){
        for (int i = 0; i < pica.size(); i++) {
            if (pica.get(i).getIme().equals(novoPice.getIme())) {
                int prethodnaKolicina = pica.get(i).getKolicina();
                pica.get(i).setKolicina(prethodnaKolicina + novoPice.getKolicina());
                return;
            }
        }
        pica.add(novoPice);
    };

    public static boolean deleteOne(Pice piceBrisanje){
        for (int i = 0; i < pica.size(); i++){
            if (pica.get(i).getIme().equals(piceBrisanje.getIme())){
                pica.remove(i);
                return true;
            }
        }
        return false;
    }

    public static boolean changeOne(String piceIme, Pice piceIzmena){
        for (int i = 0; i < pica.size(); i++){
            if (pica.get(i).getIme().equals(piceIme)){
                pica.get(i).setKolicina(piceIzmena.getKolicina());
                pica.get(i).setCena(piceIzmena.getCena());
                pica.get(i).setDetalji(piceIzmena.getDetalji());
                pica.get(i).setVrsta(piceIzmena.getVrsta());
                return true;
            }
        }
        return false;
    }
}
