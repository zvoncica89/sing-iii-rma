package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class Prijava extends AppCompatActivity {
    static ArrayList<Korisnik> korisnici;
    ArrayList<Pice> pica;
    EditText korisnickoIme, lozinka;
    Button prijaviSe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prijava);
        korisnici = (ArrayList<Korisnik>) getIntent().getSerializableExtra("korisnici");
        pica = (ArrayList<Pice>) getIntent().getSerializableExtra("pica");

        prijaviSe = findViewById(R.id.ulogujSe);
        prijaviSe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                korisnickoIme = findViewById(R.id.korisnickoIme);
                lozinka = findViewById(R.id.lozinka);
                boolean pokusaj = login(korisnickoIme.getText().toString(), lozinka.getText().toString());
                if (pokusaj == true){
                    Toast.makeText(Prijava.this, "Uspesno ste se prijavili!", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Prijava.this, PicaPrikaz.class);
                    i.putExtra("pica", pica);
                    i.putExtra("prijavljen", true);
                    startActivity(i);
//                    finish();
                } else {
                    Toast.makeText(Prijava.this, "Neuspesan pokusaj prijavljivanja", Toast.LENGTH_SHORT).show();
                    korisnickoIme.setText("");
                    lozinka.setText("");
                }
            }
        });


    }

    public static boolean login(String korisnickoIme, String lozinka){

        for (int i=0; i < korisnici.size(); i++){
            if (korisnici.get(i).getKorisnickoIme().equals(korisnickoIme) && korisnici.get(i).getLozinka().equals(lozinka)){
                return true;
            }
        }
        return false;
    }
}
