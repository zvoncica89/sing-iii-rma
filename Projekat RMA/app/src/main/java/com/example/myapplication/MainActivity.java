package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public ArrayList<Pice> pica = PicaApi.create();
    public ArrayList<Korisnik> korisnici = KorisniciApi.create();
    private boolean prijavljen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button prijava = findViewById(R.id.prijava);
        prijava.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Prijava.class);
                i.putExtra("korisnici", korisnici);
                i.putExtra("pica", pica);
                startActivity(i);
            }
        });

        Button naruci = findViewById(R.id.naruci);
        naruci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, PicaPrikaz.class);
                i.putExtra("pica", pica);
                i.putExtra("prijavljen", prijavljen);
                startActivity(i);
//                finish();
            }
        });
    }
}
