package com.example.myapplication;

import java.util.ArrayList;

public class KorisniciApi {
    private static ArrayList<Korisnik> korisnici = new ArrayList<>();
    public static ArrayList<Korisnik> create(){
        Korisnik k1 = new Korisnik("Mina", "Gusman", "mina", "mina123", "administrator");
        Korisnik k2 = new Korisnik("Maja", "Gusman", "maja", "maja123", "radnik");
        korisnici.add(k1);
        korisnici.add(k2);
        return  korisnici;
    }

    public static ArrayList<Korisnik> getAll(){
        return korisnici;
    }

    public static boolean addOne(Korisnik noviKorisnik){
        for (int i = 0; i < korisnici.size(); i++) {
            if (korisnici.get(i).getKorisnickoIme().equals(noviKorisnik.getKorisnickoIme())) {
                return false;
            }
        }
        korisnici.add(noviKorisnik);
        return true;
    };

    public static boolean deleteOne(Korisnik korisnikBrisanje){
        for (int i = 0; i < korisnici.size(); i++){
            if (korisnici.get(i).getKorisnickoIme().equals(korisnikBrisanje.getKorisnickoIme())){
                korisnici.remove(i);
                return true;
            }
        }
        return false;
    }

    public static boolean changeOne(String korisnickoIme, Korisnik korisnikIzmena){
        for (int i = 0; i < korisnici.size(); i++){
            if (korisnici.get(i).getKorisnickoIme().equals(korisnickoIme)){
                korisnici.get(i).setIme(korisnikIzmena.getIme());
                korisnici.get(i).setPrezime(korisnikIzmena.getPrezime());
                korisnici.get(i).setLozinka(korisnikIzmena.getLozinka());
                return true;
            }
        }
        return false;
    }
}
